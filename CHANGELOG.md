[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module individuals
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Migrations @StefanBrandenburger
- Controllers @StefanBrandenburger
- Base and normal models @StefanBrandenburger
- Bootstrap process for Module @StefanBrandenburger
- translation file @StefanBrandenburger
- Fixed static function editDataSingular @cmoeke

### Changed
- Namespace of ContentmetaTrait @cmoeke
- Data for new fafte parser @cmoeke
- Column sizes for fomantic @cmoeke
- Changed menu items @cmoeke

### Removed
- Unnecessary implements entry in Bootsrap.php#12 @StefanBrandenburger

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-individuals/-/tree/master
