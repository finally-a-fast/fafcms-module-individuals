<?php

namespace fafcms\individuals\abstracts\models;

use fafcms\fafcms\{
    inputs\NumberInput,
    inputs\ExtendedDropDownList,
    inputs\SwitchCheckbox,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use fafcms\individuals\{
    models\Individual,
    models\Individualgroup,
};
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the abstract model class for table "{{%individual_individualgroup}}".
 *
 * @package fafcms\individuals\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $is_main_group
 * @property int $individual_id
 * @property int $individualgroup_id
 *
 * @property Individual $individual
 * @property Individualgroup $individualgroup
 */
abstract class BaseIndividualIndividualgroup extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'individualindividualgroup';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'individualindividualgroup';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-individuals', 'IndividualIndividualgroups');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-individuals', 'IndividualIndividualgroup');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.id' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'individual_id' => static function(...$params) {
                return Individual::getOptions(...$params);
            },
            'individualgroup_id' => static function(...$params) {
                return Individualgroup::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'is_main_group' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'individual_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['individual_id'],
                'relationClassName' => Individual::class,
            ],
            'individualgroup_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['individualgroup_id'],
                'relationClassName' => Individualgroup::class,
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'is_main_group',
                        'sort' => 2,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'individual_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'individualgroup_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-is_main_group' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'is_main_group',
                                                    ],
                                                ],
                                                'field-individual_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individual_id',
                                                    ],
                                                ],
                                                'field-individualgroup_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualgroup_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%individual_individualgroup}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'boolean-is_main_group' => ['is_main_group', 'boolean'],
            'required-individual_id' => ['individual_id', 'required'],
            'required-individualgroup_id' => ['individualgroup_id', 'required'],
            'integer-individual_id' => ['individual_id', 'integer'],
            'integer-individualgroup_id' => ['individualgroup_id', 'integer'],
            'exist-individual_id' => [['individual_id'], 'exist', 'skipOnError' => true, 'targetClass' => Individual::class, 'targetAttribute' => ['individual_id' => 'id']],
            'exist-individualgroup_id' => [['individualgroup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Individualgroup::class, 'targetAttribute' => ['individualgroup_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-individuals', 'ID'),
            'is_main_group' => Yii::t('fafcms-individuals', 'Is Main Group'),
            'individual_id' => Yii::t('fafcms-individuals', 'Individual ID'),
            'individualgroup_id' => Yii::t('fafcms-individuals', 'Individualgroup ID'),
        ]);
    }

    /**
     * Gets query for [[Individual]].
     *
     * @return ActiveQuery
     */
    public function getIndividual(): ActiveQuery
    {
        return $this->hasOne(Individual::class, [
            'id' => 'individual_id',
        ]);
    }

    /**
     * Gets query for [[Individualgroup]].
     *
     * @return ActiveQuery
     */
    public function getIndividualgroup(): ActiveQuery
    {
        return $this->hasOne(Individualgroup::class, [
            'id' => 'individualgroup_id',
        ]);
    }
}
