<?php

namespace fafcms\individuals\abstracts\models;

use fafcms\fafcms\models\User;
use fafcms\fafcms\{
    inputs\DatePicker,
    inputs\DateTimePicker,
    inputs\NumberInput,
    inputs\ExtendedDropDownList,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    inputs\TimePicker,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use fafcms\individuals\models\Individual;
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%individualattributes}}".
 *
 * @package fafcms\individuals\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $name
 * @property string $input_type
 * @property string|null $schema_type
 * @property string|null $label
 * @property string|null $output_format
 * @property int $sort
 * @property string $value_type
 * @property int|null $value_int
 * @property float|null $value_decimal
 * @property string|null $value_varchar
 * @property string|null $value_text
 * @property int|null $value_bool
 * @property string|null $value_date
 * @property string|null $value_time
 * @property string|null $value_datetime
 * @property string|null $value_json
 * @property string|null $value_model_class
 * @property int|null $value_model_id
 * @property int $individual_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $createdBy
 * @property User $deletedBy
 * @property Individual $individual
 * @property User $updatedBy
 */
abstract class BaseIndividualattributes extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'individualattributes';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'individualattributes';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-individuals', 'Individualattributes');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-individuals', 'Individualattribute');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.name' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'individual_id' => static function(...$params) {
                return Individual::getOptions(...$params);
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'input_type' => [
                'type' => TextInput::class,
            ],
            'schema_type' => [
                'type' => TextInput::class,
            ],
            'label' => [
                'type' => TextInput::class,
            ],
            'output_format' => [
                'type' => Textarea::class,
            ],
            'sort' => [
                'type' => NumberInput::class,
            ],
            'value_type' => [
                'type' => TextInput::class,
            ],
            'value_int' => [
                'type' => NumberInput::class,
            ],
            'value_decimal' => [
                'type' => NumberInput::class,
            ],
            'value_varchar' => [
                'type' => TextInput::class,
            ],
            'value_text' => [
                'type' => Textarea::class,
            ],
            'value_bool' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'value_date' => [
                'type' => DatePicker::class,
            ],
            'value_time' => [
                'type' => TimePicker::class,
            ],
            'value_datetime' => [
                'type' => DateTimePicker::class,
            ],
            'value_json' => [
                'type' => Textarea::class,
            ],
            'value_model_class' => [
                'type' => TextInput::class,
            ],
            'value_model_id' => [
                'type' => NumberInput::class,
            ],
            'individual_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['individual_id'],
                'relationClassName' => Individual::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['created_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['updated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deleted_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'input_type',
                        'sort' => 3,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'schema_type',
                        'sort' => 4,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'label',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'output_format',
                        'sort' => 6,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sort',
                        'sort' => 7,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_type',
                        'sort' => 8,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_int',
                        'sort' => 9,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_decimal',
                        'sort' => 10,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_varchar',
                        'sort' => 11,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_text',
                        'sort' => 12,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_bool',
                        'sort' => 13,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_date',
                        'sort' => 14,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_time',
                        'sort' => 15,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_datetime',
                        'sort' => 16,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_json',
                        'sort' => 17,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_model_class',
                        'sort' => 18,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'value_model_id',
                        'sort' => 19,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'individual_id',
                        'sort' => 20,
                        'link' => true,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-input_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'input_type',
                                                    ],
                                                ],
                                                'field-schema_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'schema_type',
                                                    ],
                                                ],
                                                'field-label' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'label',
                                                    ],
                                                ],
                                                'field-output_format' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'output_format',
                                                    ],
                                                ],
                                                'field-sort' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sort',
                                                    ],
                                                ],
                                                'field-value_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_type',
                                                    ],
                                                ],
                                                'field-value_int' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_int',
                                                    ],
                                                ],
                                                'field-value_decimal' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_decimal',
                                                    ],
                                                ],
                                                'field-value_varchar' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_varchar',
                                                    ],
                                                ],
                                                'field-value_text' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_text',
                                                    ],
                                                ],
                                                'field-value_bool' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_bool',
                                                    ],
                                                ],
                                                'field-value_date' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_date',
                                                    ],
                                                ],
                                                'field-value_time' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_time',
                                                    ],
                                                ],
                                                'field-value_datetime' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_datetime',
                                                    ],
                                                ],
                                                'field-value_json' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_json',
                                                    ],
                                                ],
                                                'field-value_model_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_model_class',
                                                    ],
                                                ],
                                                'field-value_model_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'value_model_id',
                                                    ],
                                                ],
                                                'field-individual_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individual_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%individualattributes}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-name' => ['name', 'required'],
            'required-input_type' => ['input_type', 'required'],
            'required-sort' => ['sort', 'required'],
            'required-value_type' => ['value_type', 'required'],
            'required-individual_id' => ['individual_id', 'required'],
            'string-output_format' => ['output_format', 'string'],
            'string-value_text' => ['value_text', 'string'],
            'string-value_json' => ['value_json', 'string'],
            'integer-sort' => ['sort', 'integer'],
            'integer-value_int' => ['value_int', 'integer'],
            'integer-value_model_id' => ['value_model_id', 'integer'],
            'integer-individual_id' => ['individual_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'number-value_decimal' => ['value_decimal', 'number'],
            'boolean-value_bool' => ['value_bool', 'boolean'],
            'date-value_date' => ['value_date', 'date', 'type' => DateValidator::TYPE_DATE, 'timestampAttribute' => 'value_date', 'timestampAttributeFormat' => 'php:Y-m-d'],
            'date-value_time' => ['value_time', 'date', 'type' => DateValidator::TYPE_TIME, 'timestampAttribute' => 'value_time', 'timestampAttributeFormat' => 'php:H:i:s'],
            'date-value_datetime' => ['value_datetime', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'value_datetime', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-name' => ['name', 'string', 'max' => 255],
            'string-input_type' => ['input_type', 'string', 'max' => 255],
            'string-schema_type' => ['schema_type', 'string', 'max' => 255],
            'string-label' => ['label', 'string', 'max' => 255],
            'string-value_type' => ['value_type', 'string', 'max' => 255],
            'string-value_varchar' => ['value_varchar', 'string', 'max' => 255],
            'string-value_model_class' => ['value_model_class', 'string', 'max' => 255],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-individual_id' => [['individual_id'], 'exist', 'skipOnError' => true, 'targetClass' => Individual::class, 'targetAttribute' => ['individual_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-individuals', 'ID'),
            'name' => Yii::t('fafcms-individuals', 'Name'),
            'input_type' => Yii::t('fafcms-individuals', 'Input Type'),
            'schema_type' => Yii::t('fafcms-individuals', 'Schema Type'),
            'label' => Yii::t('fafcms-individuals', 'Label'),
            'output_format' => Yii::t('fafcms-individuals', 'Output Format'),
            'sort' => Yii::t('fafcms-individuals', 'Sort'),
            'value_type' => Yii::t('fafcms-individuals', 'Value Type'),
            'value_int' => Yii::t('fafcms-individuals', 'Value Int'),
            'value_decimal' => Yii::t('fafcms-individuals', 'Value Decimal'),
            'value_varchar' => Yii::t('fafcms-individuals', 'Value Varchar'),
            'value_text' => Yii::t('fafcms-individuals', 'Value Text'),
            'value_bool' => Yii::t('fafcms-individuals', 'Value Bool'),
            'value_date' => Yii::t('fafcms-individuals', 'Value Date'),
            'value_time' => Yii::t('fafcms-individuals', 'Value Time'),
            'value_datetime' => Yii::t('fafcms-individuals', 'Value Datetime'),
            'value_json' => Yii::t('fafcms-individuals', 'Value Json'),
            'value_model_class' => Yii::t('fafcms-individuals', 'Value Model Class'),
            'value_model_id' => Yii::t('fafcms-individuals', 'Value Model ID'),
            'individual_id' => Yii::t('fafcms-individuals', 'Individual ID'),
            'created_by' => Yii::t('fafcms-individuals', 'Created By'),
            'updated_by' => Yii::t('fafcms-individuals', 'Updated By'),
            'activated_by' => Yii::t('fafcms-individuals', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-individuals', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-individuals', 'Deleted By'),
            'created_at' => Yii::t('fafcms-individuals', 'Created At'),
            'updated_at' => Yii::t('fafcms-individuals', 'Updated At'),
            'activated_at' => Yii::t('fafcms-individuals', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-individuals', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-individuals', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[Individual]].
     *
     * @return ActiveQuery
     */
    public function getIndividual(): ActiveQuery
    {
        return $this->hasOne(Individual::class, [
            'id' => 'individual_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
