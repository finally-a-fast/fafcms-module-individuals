<?php

namespace fafcms\individuals\abstracts\models;

use fafcms\fafcms\models\User;
use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\NumberInput,
    inputs\ExtendedDropDownList,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
    models\Country,
    models\Language,
};
use fafcms\filemanager\{
    inputs\FileSelect,
    models\File,
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use fafcms\individuals\{
    models\Individualattributes,
    models\IndividualIndividualgroup,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%individual}}".
 *
 * @package fafcms\individuals\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string|null $display_start
 * @property string|null $display_end
 * @property int $sort
 * @property string|null $salutation
 * @property string|null $title
 * @property string|null $sex
 * @property string|null $firstname
 * @property string|null $lastname
 * @property string|null $username
 * @property string|null $company
 * @property string|null $position
 * @property string|null $address
 * @property string|null $zip
 * @property string|null $city
 * @property int|null $country_id
 * @property int|null $language_id
 * @property int|null $image_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property Country $country
 * @property User $createdBy
 * @property User $deletedBy
 * @property File $image
 * @property IndividualIndividualgroup[] $individualIndividualIndividualgroups
 * @property Individualattributes[] $individualIndividualattributes
 * @property Language $language
 * @property User $updatedBy
 */
abstract class BaseIndividual extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'individual';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'individual';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-individuals', 'Individuals');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-individuals', 'Individual');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['firstname'] ?? '') . ' ' . ($model['lastname'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.firstname', static::tableName() . '.lastname'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.firstname' => SORT_ASC, static::tableName() . '.lastname' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'country_id' => static function(...$params) {
                return Country::getOptions(...$params);
            },
            'language_id' => static function(...$params) {
                return Language::getOptions(...$params);
            },
            'image_id' => static function(...$params) {
                return File::getOptions(...$params);
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status'],
            ],
            'display_start' => [
                'type' => DateTimePicker::class,
            ],
            'display_end' => [
                'type' => DateTimePicker::class,
            ],
            'sort' => [
                'type' => NumberInput::class,
            ],
            'salutation' => [
                'type' => TextInput::class,
            ],
            'title' => [
                'type' => TextInput::class,
            ],
            'sex' => [
                'type' => TextInput::class,
            ],
            'firstname' => [
                'type' => TextInput::class,
            ],
            'lastname' => [
                'type' => TextInput::class,
            ],
            'username' => [
                'type' => TextInput::class,
            ],
            'company' => [
                'type' => TextInput::class,
            ],
            'position' => [
                'type' => TextInput::class,
            ],
            'address' => [
                'type' => TextInput::class,
            ],
            'zip' => [
                'type' => TextInput::class,
            ],
            'city' => [
                'type' => TextInput::class,
            ],
            'country_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['country_id'],
                'relationClassName' => Country::class,
            ],
            'language_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['language_id'],
                'relationClassName' => Language::class,
            ],
            'image_id' => [
                'type' => FileSelect::class,
                'mediatypes' => [
                    'image',
                ],
                'external' => false,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['created_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['updated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deleted_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'firstname',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'lastname',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'display_start',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'display_end',
                        'sort' => 6,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sort',
                        'sort' => 7,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'salutation',
                        'sort' => 8,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'title',
                        'sort' => 9,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sex',
                        'sort' => 10,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'username',
                        'sort' => 11,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'company',
                        'sort' => 12,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'position',
                        'sort' => 13,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'address',
                        'sort' => 14,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'zip',
                        'sort' => 15,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'city',
                        'sort' => 16,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'country_id',
                        'sort' => 17,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'language_id',
                        'sort' => 18,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'image_id',
                        'sort' => 19,
                        'link' => true,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-display_start' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                'field-display_end' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                                'field-sort' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sort',
                                                    ],
                                                ],
                                                'field-salutation' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'salutation',
                                                    ],
                                                ],
                                                'field-title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                'field-sex' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sex',
                                                    ],
                                                ],
                                                'field-firstname' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'firstname',
                                                    ],
                                                ],
                                                'field-lastname' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'lastname',
                                                    ],
                                                ],
                                                'field-username' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'username',
                                                    ],
                                                ],
                                                'field-company' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'company',
                                                    ],
                                                ],
                                                'field-position' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'position',
                                                    ],
                                                ],
                                                'field-address' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'address',
                                                    ],
                                                ],
                                                'field-zip' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'zip',
                                                    ],
                                                ],
                                                'field-city' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'city',
                                                    ],
                                                ],
                                                'field-country_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'country_id',
                                                    ],
                                                ],
                                                'field-language_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'language_id',
                                                    ],
                                                ],
                                                'field-image_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'image_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%individual}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-status' => ['status', 'required'],
            'required-sort' => ['sort', 'required'],
            'date-display_start' => ['display_start', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_start', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-display_end' => ['display_end', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_end', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'integer-sort' => ['sort', 'integer'],
            'integer-country_id' => ['country_id', 'integer'],
            'integer-language_id' => ['language_id', 'integer'],
            'integer-image_id' => ['image_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-salutation' => ['salutation', 'string', 'max' => 255],
            'string-title' => ['title', 'string', 'max' => 255],
            'string-sex' => ['sex', 'string', 'max' => 255],
            'string-firstname' => ['firstname', 'string', 'max' => 255],
            'string-lastname' => ['lastname', 'string', 'max' => 255],
            'string-username' => ['username', 'string', 'max' => 255],
            'string-company' => ['company', 'string', 'max' => 255],
            'string-position' => ['position', 'string', 'max' => 255],
            'string-address' => ['address', 'string', 'max' => 255],
            'string-zip' => ['zip', 'string', 'max' => 255],
            'string-city' => ['city', 'string', 'max' => 255],
            'exist-country_id' => [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::class, 'targetAttribute' => ['country_id' => 'id']],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-image_id' => [['image_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::class, 'targetAttribute' => ['image_id' => 'id']],
            'exist-language_id' => [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::class, 'targetAttribute' => ['language_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-individuals', 'ID'),
            'status' => Yii::t('fafcms-individuals', 'Status'),
            'display_start' => Yii::t('fafcms-individuals', 'Display Start'),
            'display_end' => Yii::t('fafcms-individuals', 'Display End'),
            'sort' => Yii::t('fafcms-individuals', 'Sort'),
            'salutation' => Yii::t('fafcms-individuals', 'Salutation'),
            'title' => Yii::t('fafcms-individuals', 'Title'),
            'sex' => Yii::t('fafcms-individuals', 'Sex'),
            'firstname' => Yii::t('fafcms-individuals', 'Firstname'),
            'lastname' => Yii::t('fafcms-individuals', 'Lastname'),
            'username' => Yii::t('fafcms-individuals', 'Username'),
            'company' => Yii::t('fafcms-individuals', 'Company'),
            'position' => Yii::t('fafcms-individuals', 'Position'),
            'address' => Yii::t('fafcms-individuals', 'Address'),
            'zip' => Yii::t('fafcms-individuals', 'Zip'),
            'city' => Yii::t('fafcms-individuals', 'City'),
            'country_id' => Yii::t('fafcms-individuals', 'Country ID'),
            'language_id' => Yii::t('fafcms-individuals', 'Language ID'),
            'image_id' => Yii::t('fafcms-individuals', 'Image ID'),
            'created_by' => Yii::t('fafcms-individuals', 'Created By'),
            'updated_by' => Yii::t('fafcms-individuals', 'Updated By'),
            'activated_by' => Yii::t('fafcms-individuals', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-individuals', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-individuals', 'Deleted By'),
            'created_at' => Yii::t('fafcms-individuals', 'Created At'),
            'updated_at' => Yii::t('fafcms-individuals', 'Updated At'),
            'activated_at' => Yii::t('fafcms-individuals', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-individuals', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-individuals', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[Country]].
     *
     * @return ActiveQuery
     */
    public function getCountry(): ActiveQuery
    {
        return $this->hasOne(Country::class, [
            'id' => 'country_id',
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[Image]].
     *
     * @return ActiveQuery
     */
    public function getImage(): ActiveQuery
    {
        return $this->hasOne(File::class, [
            'id' => 'image_id',
        ]);
    }

    /**
     * Gets query for [[IndividualIndividualIndividualgroups]].
     *
     * @return ActiveQuery
     */
    public function getIndividualIndividualIndividualgroups(): ActiveQuery
    {
        return $this->hasMany(IndividualIndividualgroup::class, [
            'individual_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[IndividualIndividualattributes]].
     *
     * @return ActiveQuery
     */
    public function getIndividualIndividualattributes(): ActiveQuery
    {
        return $this->hasMany(Individualattributes::class, [
            'individual_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Language]].
     *
     * @return ActiveQuery
     */
    public function getLanguage(): ActiveQuery
    {
        return $this->hasOne(Language::class, [
            'id' => 'language_id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
