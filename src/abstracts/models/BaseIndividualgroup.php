<?php

namespace fafcms\individuals\abstracts\models;

use fafcms\fafcms\models\User;
use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\NumberInput,
    inputs\ExtendedDropDownList,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionTrait,
};
use fafcms\individuals\models\IndividualIndividualgroup;
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%individualgroup}}".
 *
 * @package fafcms\individuals\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string|null $display_start
 * @property string|null $display_end
 * @property string $name
 * @property int $sort
 * @property int|null $parent_id
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property User $createdBy
 * @property User $deletedBy
 * @property IndividualIndividualgroup[] $individualgroupIndividualIndividualgroups
 * @property Individualgroup $parent
 * @property Individualgroup[] $parentIndividualgroups
 * @property User $updatedBy
 */
abstract class BaseIndividualgroup extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return 'individualgroup';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'individualgroup';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-individuals', 'Individualgroups');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-individuals', 'Individualgroup');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptions(bool $empty = true, array $select = null, array $sort = null, array $where = null, array $joinWith = null, string $emptyLabel = null): array
    {
        $options = Yii::$app->dataCache->map(
            static::class,
            array_merge([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ], $select ?? []),
            'id',
            static function ($item) {
                return static::extendedLabel($item);
            },
            null,
            $sort ?? [static::tableName() . '.name' => SORT_ASC],
            $where,
            $joinWith
        );

        return OptionTrait::getOptions($empty, $select, $sort, $where, $joinWith, $emptyLabel) + $options;
    }
    //endregion OptionTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
            'parent_id' => static function(...$params) {
                return static::getOptions(...$params);
            },
            'created_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'updated_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
            'deleted_by' => static function(...$params) {
                return User::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->attributeOptions()['status'],
            ],
            'display_start' => [
                'type' => DateTimePicker::class,
            ],
            'display_end' => [
                'type' => DateTimePicker::class,
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'sort' => [
                'type' => NumberInput::class,
            ],
            'parent_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['parent_id'],
                'relationClassName' => static::class,
            ],
            'created_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['created_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['updated_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->attributeOptions()['deleted_by'],
                'relationClassName' => User::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'display_start',
                        'sort' => 4,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'display_end',
                        'sort' => 5,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'sort',
                        'sort' => 6,
                    ],
                ],
                [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'parent_id',
                        'sort' => 7,
                        'link' => true,
                    ],
                ],
                [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-display_start' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_start',
                                                    ],
                                                ],
                                                'field-display_end' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'display_end',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-sort' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'sort',
                                                    ],
                                                ],
                                                'field-parent_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'parent_id',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%individualgroup}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-status' => ['status', 'required'],
            'required-name' => ['name', 'required'],
            'required-sort' => ['sort', 'required'],
            'date-display_start' => ['display_start', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_start', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-display_end' => ['display_end', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'display_end', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'integer-sort' => ['sort', 'integer'],
            'integer-parent_id' => ['parent_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-name' => ['name', 'string', 'max' => 255],
            'exist-created_by' => [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            'exist-deleted_by' => [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            'exist-parent_id' => [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => static::class, 'targetAttribute' => ['parent_id' => 'id']],
            'exist-updated_by' => [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-individuals', 'ID'),
            'status' => Yii::t('fafcms-individuals', 'Status'),
            'display_start' => Yii::t('fafcms-individuals', 'Display Start'),
            'display_end' => Yii::t('fafcms-individuals', 'Display End'),
            'name' => Yii::t('fafcms-individuals', 'Name'),
            'sort' => Yii::t('fafcms-individuals', 'Sort'),
            'parent_id' => Yii::t('fafcms-individuals', 'Parent ID'),
            'created_by' => Yii::t('fafcms-individuals', 'Created By'),
            'updated_by' => Yii::t('fafcms-individuals', 'Updated By'),
            'activated_by' => Yii::t('fafcms-individuals', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-individuals', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-individuals', 'Deleted By'),
            'created_at' => Yii::t('fafcms-individuals', 'Created At'),
            'updated_at' => Yii::t('fafcms-individuals', 'Updated At'),
            'activated_at' => Yii::t('fafcms-individuals', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-individuals', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-individuals', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return ActiveQuery
     */
    public function getCreatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'created_by',
        ]);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return ActiveQuery
     */
    public function getDeletedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'deleted_by',
        ]);
    }

    /**
     * Gets query for [[IndividualgroupIndividualIndividualgroups]].
     *
     * @return ActiveQuery
     */
    public function getIndividualgroupIndividualIndividualgroups(): ActiveQuery
    {
        return $this->hasMany(IndividualIndividualgroup::class, [
            'individualgroup_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return ActiveQuery
     */
    public function getParent(): ActiveQuery
    {
        return $this->hasOne(static::class, [
            'id' => 'parent_id',
        ]);
    }

    /**
     * Gets query for [[ParentIndividualgroups]].
     *
     * @return ActiveQuery
     */
    public function getParentIndividualgroups(): ActiveQuery
    {
        return $this->hasMany(static::class, [
            'parent_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return ActiveQuery
     */
    public function getUpdatedBy(): ActiveQuery
    {
        return $this->hasOne(User::class, [
            'id' => 'updated_by',
        ]);
    }
}
