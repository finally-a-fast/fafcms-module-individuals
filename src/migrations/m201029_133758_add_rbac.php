<?php

namespace fafcms\individuals\migrations;

use fafcms\fafcms\models\AuthModel;
use fafcms\helpers\ClassFinder;
use fafcms\individuals\models\Individual;
use fafcms\individuals\models\Individualgroup;
use fafcms\updater\base\Migration;
use Yii;

/**
 * Class m201029_133758_add_rbac
 */
class m201029_133758_add_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $auth             = Yii::$app->getAuthManager();
        $modelClasses     = $this->getModelClasses();
        $admin            = $auth->getRole('admin');
        $permissions      = [
            'view'   => $auth->getPermission('view'),
            'create' => $auth->getPermission('create'),
            'edit'   => $auth->getPermission('edit'),
            'delete' => $auth->getPermission('delete'),
        ];

        if ($admin !== null) {
            foreach ($modelClasses as $modelClass) {
                foreach ($permissions as $permission) {
                    if ($permission !== null) {
                        $authModel = new AuthModel([
                            'type'                 => AuthModel::TYPE_ROLE,
                            'permission_item_name' => $permission->name,
                            'role_item_name'       => $admin->name,
                            'model_type'           => AuthModel::MODEL_TYPE_TABLE,
                            'model_class'          => $modelClass,
                        ]);

                        if (!$authModel->save()) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        echo "m201029_133758_add_rbac cannot be reverted.\n";

        return true;
    }

    private function getModelClasses(): array
    {
        return [
            Individual::class,
            Individualgroup::class,
        ];
    }
}
