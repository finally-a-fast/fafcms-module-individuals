<?php

namespace fafcms\individuals\migrations;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\fafcms\models\Country;
use fafcms\fafcms\models\Language;
use fafcms\fafcms\models\User;
use fafcms\filemanager\models\File;
use fafcms\individuals\Bootstrap;
use fafcms\sitemanager\models\Contentmeta;
use fafcms\updater\base\Migration;

/**
 * Class m201028_145415_individuals_init
 */
class m201028_145415_individuals_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp(): bool
    {
        $tableNames           = $this->getTableNames();
        $userTableName        = InjectorComponent::getClass(User::class)::tableName();
        $countryTableName     = InjectorComponent::getClass(Country::class)::tableName();
        $languageTableName    = InjectorComponent::getClass(Language::class)::tableName();
        $imageTableName       = InjectorComponent::getClass(File::class)::tableName();
        $tableOptions         = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($tableNames['individual'], [
            'id'             => $this->primaryKey(10)->unsigned(),
            'status'         => $this->string(255)->notNull(),
            'display_start'  => $this->datetime()->null()->defaultValue(null),
            'display_end'    => $this->datetime()->null()->defaultValue(null),
            'sort'           => $this->integer(10)->unsigned()->notNull(),
            'salutation'     => $this->string(255)->null()->defaultValue(null),
            'title'          => $this->string(255)->null()->defaultValue(null),
            'sex'            => $this->string(255)->null()->defaultValue(null),
            'firstname'      => $this->string(255)->null()->defaultValue(null),
            'lastname'       => $this->string(255)->null()->defaultValue(null),
            'username'       => $this->string(255)->null()->defaultValue(null),
            'company'        => $this->string(255)->null()->defaultValue(null),
            'position'       => $this->string(255)->null()->defaultValue(null),
            'address'        => $this->string(255)->null()->defaultValue(null),
            'zip'            => $this->string(255)->null()->defaultValue(null),
            'city'           => $this->string(255)->null()->defaultValue(null),
            'country_id'     => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'language_id'    => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'image_id'       => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by'     => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by'     => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by'   => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by'     => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at'     => $this->datetime()->null()->defaultValue(null),
            'updated_at'     => $this->datetime()->null()->defaultValue(null),
            'activated_at'   => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at'     => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createTable($tableNames['individualgroup'], [
            'id'             => $this->primaryKey(10)->unsigned(),
            'status'         => $this->string(255)->notNull(),
            'display_start'  => $this->datetime()->null()->defaultValue(null),
            'display_end'    => $this->datetime()->null()->defaultValue(null),
            'name'           => $this->string(255)->notNull(),
            'sort'           => $this->integer(10)->unsigned()->notNull(),
            'parent_id'      => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by'     => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by'     => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by'   => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by'     => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at'     => $this->datetime()->null()->defaultValue(null),
            'updated_at'     => $this->datetime()->null()->defaultValue(null),
            'activated_at'   => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at'     => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createTable($tableNames['individual_individualgroup'], [
            'id'                 => $this->primaryKey(10)->unsigned(),
            'is_main_group'      => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'individual_id'      => $this->integer(10)->unsigned()->notNull(),
            'individualgroup_id' => $this->integer(10)->unsigned()->notNull(),
        ], $tableOptions);

        $this->createTable($tableNames['individualattributes'], [
            'id'                => $this->primaryKey(10)->unsigned(),
            'name'              => $this->string(255)->notNull(),
            'input_type'        => $this->string(255)->notNull(),
            'schema_type'       => $this->string(255)->null()->defaultValue(null),
            'label'             => $this->string(255)->null()->defaultValue(null),
            'output_format'     => $this->text()->null()->defaultValue(null),
            'sort'              => $this->integer(10)->unsigned()->notNull(),
            'value_type'        => $this->string(255)->notNull(),
            'value_int'         => $this->integer(11)->null()->defaultValue(null),
            'value_decimal'     => $this->decimal(16,4)->null()->defaultValue(null),
            'value_varchar'     => $this->string(255)->null()->defaultValue(null),
            'value_text'        => $this->text()->null()->defaultValue(null),
            'value_bool'        => $this->tinyInteger(1)->null()->defaultValue(null),
            'value_date'        => $this->date()->null()->defaultValue(null),
            'value_time'        => $this->time()->null()->defaultValue(null),
            'value_datetime'    => $this->dateTime()->null()->defaultValue(null),
            'value_json'        => $this->text()->null()->defaultValue(null),
            'value_model_class' => $this->string(255)->null()->defaultValue(null),
            'value_model_id'    => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'individual_id'     => $this->integer(10)->unsigned()->notNull(),
            'created_by'        => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by'        => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by'      => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by'    => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by'        => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at'        => $this->datetime()->null()->defaultValue(null),
            'updated_at'        => $this->datetime()->null()->defaultValue(null),
            'activated_at'      => $this->datetime()->null()->defaultValue(null),
            'deactivated_at'    => $this->datetime()->null()->defaultValue(null),
            'deleted_at'        => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-individual-created_by', $tableNames['individual'], ['created_by'], false);
        $this->createIndex('idx-individual-updated_by', $tableNames['individual'], ['updated_by'], false);
        $this->createIndex('idx-individual-deleted_by', $tableNames['individual'], ['deleted_by'], false);
        $this->createIndex('idx-individual-country_id', $tableNames['individual'], ['country_id'], false);
        $this->createIndex('idx-individual-language_id', $tableNames['individual'], ['language_id'], false);
        $this->createIndex('idx-individual-image_id', $tableNames['individual'], ['image_id'], false);

        $this->createIndex('idx-individualgroup-created_by', $tableNames['individualgroup'], ['created_by'], false);
        $this->createIndex('idx-individualgroup-updated_by', $tableNames['individualgroup'], ['updated_by'], false);
        $this->createIndex('idx-individualgroup-deleted_by', $tableNames['individualgroup'], ['deleted_by'], false);
        $this->createIndex('idx-individualgroup-parent_id', $tableNames['individualgroup'], ['parent_id'], false);

        $this->createIndex('idx-individual_individualgroup-individual_id', $tableNames['individual_individualgroup'], ['individual_id'], false);
        $this->createIndex('idx-individual_individualgroup-individualgroup_id', $tableNames['individual_individualgroup'], ['individualgroup_id'], false);

        $this->createIndex('idx-individualattributes-created_by', $tableNames['individualattributes'], ['created_by'], false);
        $this->createIndex('idx-individualattributes-updated_by', $tableNames['individualattributes'], ['updated_by'], false);
        $this->createIndex('idx-individualattributes-deleted_by', $tableNames['individualattributes'], ['deleted_by'], false);
        $this->createIndex('idx-individualattributes-individual_id', $tableNames['individualattributes'], ['individual_id'], false);

        $this->addForeignKey('fk-individual-created_by', $tableNames['individual'], 'created_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individual-updated_by', $tableNames['individual'], 'updated_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individual-deleted_by', $tableNames['individual'], 'deleted_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individual-country_id', $tableNames['individual'], 'country_id', $countryTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individual-language_id', $tableNames['individual'], 'language_id', $languageTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individual-image_id', $tableNames['individual'], 'image_id', $imageTableName, 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-individualgroup-created_by', $tableNames['individualgroup'], 'created_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individualgroup-updated_by', $tableNames['individualgroup'], 'updated_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individualgroup-deleted_by', $tableNames['individualgroup'], 'deleted_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individualgroup-parent_id', $tableNames['individualgroup'], 'parent_id', $tableNames['individualgroup'], 'id', 'SET NULL', 'CASCADE');

        $this->addForeignKey('fk-individual_individualgroup-individual_id', $tableNames['individual_individualgroup'], 'individual_id', $tableNames['individual'], 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('fk-individual_individualgroup-individualgroup_id', $tableNames['individual_individualgroup'], 'individualgroup_id', $tableNames['individualgroup'], 'id', 'RESTRICT', 'CASCADE');

        $this->addForeignKey('fk-individualattributes-created_by', $tableNames['individualattributes'], 'created_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individualattributes-updated_by', $tableNames['individualattributes'], 'updated_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individualattributes-deleted_by', $tableNames['individualattributes'], 'deleted_by', $userTableName, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('fk-individualattributes-individual_id', $tableNames['individualattributes'], 'individual_id', $tableNames['individual'], 'id', 'RESTRICT', 'CASCADE');

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown(): bool
    {
        $tableNames = $this->getTableNames();

        $this->dropForeignKey('fk-individual-created_by', $tableNames['individual']);
        $this->dropForeignKey('fk-individual-updated_by', $tableNames['individual']);
        $this->dropForeignKey('fk-individual-deleted_by', $tableNames['individual']);
        $this->dropForeignKey('fk-individual-country_id', $tableNames['individual']);
        $this->dropForeignKey('fk-individual-language_id', $tableNames['individual']);
        $this->dropForeignKey('fk-individual-image_id', $tableNames['individual']);

        $this->dropForeignKey('fk-individualgroup-created_by', $tableNames['individualgroup']);
        $this->dropForeignKey('fk-individualgroup-updated_by', $tableNames['individualgroup']);
        $this->dropForeignKey('fk-individualgroup-deleted_by', $tableNames['individualgroup']);
        $this->dropForeignKey('fk-individualgroup-parent_id', $tableNames['individualgroup']);

        $this->dropForeignKey('fk-individual_individualgroup-individual_id', $tableNames['individual_individualgroup']);
        $this->dropForeignKey('fk-individual_individualgroup-individualgroup_id', $tableNames['individual_individualgroup']);

        $this->dropForeignKey('fk-individualattributes-created_by', $tableNames['individualattributes']);
        $this->dropForeignKey('fk-individualattributes-updated_by', $tableNames['individualattributes']);
        $this->dropForeignKey('fk-individualattributes-deleted_by', $tableNames['individualattributes']);
        $this->dropForeignKey('fk-individualattributes-individual_id', $tableNames['individualattributes']);

        $this->dropIndex('idx-individual-created_by', $tableNames['individual']);
        $this->dropIndex('idx-individual-updated_by', $tableNames['individual']);
        $this->dropIndex('idx-individual-deleted_by', $tableNames['individual']);
        $this->dropIndex('idx-individual-country_id', $tableNames['individual']);
        $this->dropIndex('idx-individual-language_id', $tableNames['individual']);
        $this->dropIndex('idx-individual-image_id', $tableNames['individual']);

        $this->dropIndex('idx-individualgroup-created_by', $tableNames['individualgroup']);
        $this->dropIndex('idx-individualgroup-updated_by', $tableNames['individualgroup']);
        $this->dropIndex('idx-individualgroup-deleted_by', $tableNames['individualgroup']);
        $this->dropIndex('idx-individualgroup-parent_id', $tableNames['individualgroup']);

        $this->dropIndex('idx-individual_individualgroup-individual_id', $tableNames['individual_individualgroup']);
        $this->dropIndex('idx-individual_individualgroup-individualgroup_id', $tableNames['individual_individualgroup']);

        $this->dropIndex('idx-individualattributes-created_by', $tableNames['individualattributes']);
        $this->dropIndex('idx-individualattributes-updated_by', $tableNames['individualattributes']);
        $this->dropIndex('idx-individualattributes-deleted_by', $tableNames['individualattributes']);
        $this->dropIndex('idx-individualattributes-individual_id', $tableNames['individualattributes']);

        foreach ($tableNames as $table) {
            $this->dropTable($table);
        }

        return true;
    }

    private function getTableNames(): array
    {
        $tablePrefix = Bootstrap::$tablePrefix;

        return [
            'individualattributes'       => '{{%' . $tablePrefix . 'individualattributes}}',
            'individual_individualgroup' => '{{%' . $tablePrefix . 'individual_individualgroup}}',
            'individualgroup'            => '{{%' . $tablePrefix . 'individualgroup}}',
            'individual'                 => '{{%' . $tablePrefix . 'individual}}',
        ];
    }
}
