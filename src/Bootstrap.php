<?php

namespace fafcms\individuals;

use fafcms\helpers\{
    abstractions\PluginBootstrap,
    abstractions\PluginModule,
};
use fafcms\fafcms\components\FafcmsComponent;
use fafcms\individuals\{
    models\Individual,
    models\Individualgroup,
};
use Yii;
use yii\base\Application;
use yii\helpers\ArrayHelper;
use yii\i18n\PhpMessageSource;

/**
 * Class Bootstrap
 *
 * @package fafcms\individuals
 */
class Bootstrap extends PluginBootstrap
{
    public static $id          = 'fafcms-individuals';
    public static $tablePrefix = 'fafcms-individuals_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations[static::$id])) {
            $app->i18n->translations[static::$id] = [
                'class'            => PhpMessageSource::class,
                'basePath'         => __DIR__ . '/../translations',
                'forceTranslation' => true,
            ];
        }

        return parent::bootstrapTranslations($app, $module);
    }

    protected function bootstrapApp(Application $app, PluginModule $module): bool
    {
        if ($app->fafcms->getIsInstalled()) {
            $app->fafcmsParser->data[] = [
                'allIndividuals' => function () {
                    return ArrayHelper::index(Individual::find()->all(), 'id');
                },
                'allIndividualgroups' => function () {
                    return ArrayHelper::index(Individualgroup::find()->all(), 'id');
                },
            ];
        }

        return parent::bootstrapApp($app, $module);
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        if ($app->fafcms->getIsInstalled()) {
            Yii::$app->view->addNavigationItems(
                FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
                [
                    'project' => [
                        'items' => [
                            'individuals'    => [
                                'after' => 'tags',
                                'label' => Yii::t(static::$id,  'Individuals'),
                                'icon' => 'account',
                                'items' => [
                                    'individuals' => [
                                        'label'   => Individual::instance()->getEditData()['plural'],
                                        'icon'    => Individual::instance()->getEditData()['icon'],
                                        'url'     => [Individual::instance()->getEditData()['url'] . '/index'],
                                    ],
                                    'individualgroups' => [
                                        'label'   => Individualgroup::instance()->getEditData()['plural'],
                                        'icon'    => Individualgroup::instance()->getEditData()['icon'],
                                        'url'     => [Individualgroup::instance()->getEditData()['url'] . '/index'],
                                    ],
                                ]
                            ],
                        ],
                    ],
                ]
            );

            $app->fafcms->addBackendUrlRules(static::$id, [
                '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
                '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>'                     => '<controller>/<action>',
            ]);
        }

        return parent::bootstrapWebApp($app, $module);
    }
}
