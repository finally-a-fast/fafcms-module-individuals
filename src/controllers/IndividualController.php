<?php


namespace fafcms\individuals\controllers;


use fafcms\helpers\DefaultController;
use fafcms\individuals\models\Individual;

/**
 * Class IndividualController
 *
 * @package fafcms\individuals\controller
 */
class IndividualController extends DefaultController
{
    public static $modelClass = Individual::class;
}
