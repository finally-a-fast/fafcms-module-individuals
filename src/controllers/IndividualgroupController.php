<?php


namespace fafcms\individuals\controllers;


use fafcms\helpers\DefaultController;
use fafcms\individuals\models\Individualgroup;

/**
 * Class IndividualgroupController
 *
 * @package fafcms\individuals\controller
 */
class IndividualgroupController extends DefaultController
{
    public static $modelClass = Individualgroup::class;
}
