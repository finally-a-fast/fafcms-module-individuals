<?php

namespace fafcms\individuals\models;

use \fafcms\individuals\abstracts\models\BaseIndividualgroup;
use fafcms\individuals\Bootstrap;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%individualgroup}}".
 *
 * @package fafcms\individuals\models
 */
class Individualgroup extends BaseIndividualgroup
{
    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return '/' . Bootstrap::$id . '/individualgroup';
    }

    /**
     * {@inheritdoc}
     */
    public static function editDataIcon($model): string
    {
        return 'account-group';
    }
    //endregion BeautifulModelTrait implementation
}
