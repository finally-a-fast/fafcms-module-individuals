<?php

namespace fafcms\individuals\models;

use \fafcms\individuals\abstracts\models\BaseIndividualIndividualgroup;

/**
 * This is the model class for table "{{%individual_individualgroup}}".
 *
 * @package fafcms\individuals\models
 */
class IndividualIndividualgroup extends BaseIndividualIndividualgroup
{

}
