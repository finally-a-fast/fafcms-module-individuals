<?php

namespace fafcms\individuals\models;

use fafcms\fafcms\components\InjectorComponent;
use fafcms\individuals\abstracts\models\BaseIndividual;
use fafcms\individuals\Bootstrap;
use fafcms\sitemanager\traits\ContentmetaTrait;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%individual}}".
 *
 * @package fafcms\individuals\models
 */
class Individual extends BaseIndividual
{
    use ContentmetaTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return '/' . Bootstrap::$id . '/individual';
    }

    public static function editDataIcon($model): string
    {
        return 'account';
    }
    //endregion BeautifulModelTrait implementation

    //region IndexViewInterface implementation
    /**
     * @return array[]
     */
    public static function editView(): array
    {
        $config = parent::editView()['default'];
        return [
            'default' => [
                'tab-1' => $config['tab-1'],
//                'attributes-tab'  => InjectorComponent::getClass(Individualattributes::class)::editView()['individualView']['tab-1'],
            ],
        ];
    }
}
