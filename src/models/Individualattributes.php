<?php

namespace fafcms\individuals\models;

use fafcms\fafcms\items\Card;
use fafcms\fafcms\items\Column;
use fafcms\fafcms\items\FormField;
use fafcms\fafcms\items\Row;
use fafcms\fafcms\items\Tab;
use fafcms\helpers\ClassFinder;
//use fafcms\helpers\traits\MixedValueTrait;
use fafcms\individuals\abstracts\models\BaseIndividualattributes;
use fafcms\individuals\Bootstrap;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%individualattributes}}".
 *
 * @package fafcms\individuals\models
 */
class Individualattributes extends BaseIndividualattributes
{
//    use MixedValueTrait;

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return array_merge(parent::attributeOptions(), [
            'input_type' => static function(...$params) {
                $finder = new ClassFinder();

                $inputClasses = [
                    $finder->findClasses(Yii::getAlias('@project/inputs'))
                ];

                foreach (Yii::$app->fafcms->getLoadedPluginPaths() as $loadedPluginPath) {
                    $inputClasses[] = $finder->findClasses($loadedPluginPath . '/inputs');
                }

                $inputClasses = array_merge([], ...$inputClasses);

                return ArrayHelper::map($inputClasses, 'fullyQualifiedClassName', 'class');
            },
            'schema_type' => [
                'additionalType'            => Yii::t('fafcms-individuals', 'additionalType'),
                'alternateName'             => Yii::t('fafcms-individuals', 'alternateName'),
                'description'               => Yii::t('fafcms-individuals', 'description'),
                'disambiguatingDescription' => Yii::t('fafcms-individuals', 'disambiguatingDescription'),
                'identifier'                => Yii::t('fafcms-individuals', 'identifier'),
                'image'                     => Yii::t('fafcms-individuals', 'image'),
                'mainEntityOfPage'          => Yii::t('fafcms-individuals', 'mainEntityOfPage'),
                'name'                      => Yii::t('fafcms-individuals', 'name'),
                'potentialAction'           => Yii::t('fafcms-individuals', 'potentialAction'),
                'sameAs'                    => Yii::t('fafcms-individuals', 'sameAs'),
                'subjectOf'                 => Yii::t('fafcms-individuals', 'subjectOf'),
                'url'                       => Yii::t('fafcms-individuals', 'url'),
                'additionalName'            => Yii::t('fafcms-individuals', 'additionalName'),
                'address'                   => Yii::t('fafcms-individuals', 'address'),
                'affiliation'               => Yii::t('fafcms-individuals', 'affiliation'),
                'alumniOf'                  => Yii::t('fafcms-individuals', 'alumniOf'),
                'award individual'          => Yii::t('fafcms-individuals', 'award individual'),
                'birthDate'                 => Yii::t('fafcms-individuals', 'birthDate'),
                'birthPlace'                => Yii::t('fafcms-individuals', 'birthPlace'),
                'brand'                     => Yii::t('fafcms-individuals', 'brand'),
                'callSign'                  => Yii::t('fafcms-individuals', 'callSign'),
                'children'                  => Yii::t('fafcms-individuals', 'children'),
                'colleague'                 => Yii::t('fafcms-individuals', 'colleague'),
                'contactPoint'              => Yii::t('fafcms-individuals', 'contactPoint'),
                'deathDate'                 => Yii::t('fafcms-individuals', 'deathDate'),
                'deathPlace'                => Yii::t('fafcms-individuals', 'deathPlace'),
                'duns'                      => Yii::t('fafcms-individuals', 'duns'),
                'email'                     => Yii::t('fafcms-individuals', 'email'),
                'familyName'                => Yii::t('fafcms-individuals', 'familyName'),
                'faxNumber'                 => Yii::t('fafcms-individuals', 'faxNumber'),
                'follows'                   => Yii::t('fafcms-individuals', 'follows'),
                'funder'                    => Yii::t('fafcms-individuals', 'funder'),
                'gender'                    => Yii::t('fafcms-individuals', 'gender'),
                'givenName'                 => Yii::t('fafcms-individuals', 'givenName'),
                'globalLocationNumber'      => Yii::t('fafcms-individuals', 'globalLocationNumber'),
                'hasCredential'             => Yii::t('fafcms-individuals', 'hasCredential'),
                'hasOccupation'             => Yii::t('fafcms-individuals', 'hasOccupation'),
                'hasOfferCatalog'           => Yii::t('fafcms-individuals', 'hasOfferCatalog'),
                'hasPOS'                    => Yii::t('fafcms-individuals', 'hasPOS'),
                'height'                    => Yii::t('fafcms-individuals', 'height'),
                'homeLocation'              => Yii::t('fafcms-individuals', 'homeLocation'),
                'honorificPrefix'           => Yii::t('fafcms-individuals', 'honorificPrefix'),
                'honorificSuffix'           => Yii::t('fafcms-individuals', 'honorificSuffix'),
                'interactionStatistic'      => Yii::t('fafcms-individuals', 'interactionStatistic'),
                'isicV4'                    => Yii::t('fafcms-individuals', 'isicV4'),
                'jobTitle'                  => Yii::t('fafcms-individuals', 'jobTitle'),
                'knows'                     => Yii::t('fafcms-individuals', 'knows'),
                'knowsAbout'                => Yii::t('fafcms-individuals', 'knowsAbout'),
                'knowsLanguage'             => Yii::t('fafcms-individuals', 'knowsLanguage'),
                'makesOffer'                => Yii::t('fafcms-individuals', 'makesOffer'),
                'memberOf'                  => Yii::t('fafcms-individuals', 'memberOf'),
                'naics'                     => Yii::t('fafcms-individuals', 'naics'),
                'nationality'               => Yii::t('fafcms-individuals', 'nationality'),
                'netWorth'                  => Yii::t('fafcms-individuals', 'netWorth'),
                'owns'                      => Yii::t('fafcms-individuals', 'owns'),
                'parent'                    => Yii::t('fafcms-individuals', 'parent'),
                'performerIn'               => Yii::t('fafcms-individuals', 'performerIn'),
                'publishingPrinciples'      => Yii::t('fafcms-individuals', 'publishingPrinciples'),
                'relatedTo'                 => Yii::t('fafcms-individuals', 'relatedTo'),
                'seeks'                     => Yii::t('fafcms-individuals', 'seeks'),
                'sibling'                   => Yii::t('fafcms-individuals', 'sibling'),
                'sponsor'                   => Yii::t('fafcms-individuals', 'sponsor'),
                'spouse'                    => Yii::t('fafcms-individuals', 'spouse'),
                'taxID'                     => Yii::t('fafcms-individuals', 'taxID'),
                'telephone'                 => Yii::t('fafcms-individuals', 'telephone'),
                'vatID'                     => Yii::t('fafcms-individuals', 'vatID'),
                'weight'                    => Yii::t('fafcms-individuals', 'weight'),
                'workLocation'              => Yii::t('fafcms-individuals', 'workLocation'),
                'worksFor'                  => Yii::t('fafcms-individuals', 'worksFor'),
            ],
            ''
        ]);
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return array_merge(parent::getFieldConfig(), [

        ]);
    }
    //endregion FieldConfigInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return array_merge(parent::editView(), [
            'individualView' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            Bootstrap::$id,
                            'Attributes',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.name',
                                                    ],
                                                ],
                                                'field-input_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.input_type',
                                                    ],
                                                ],
                                                'field-schema_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.schema_type',
                                                    ],
                                                ],
                                                'field-label' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.label',
                                                    ],
                                                ],
                                                'field-output_format' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.output_format',
                                                    ],
                                                ],
                                                'field-sort' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.sort',
                                                    ],
                                                ],
                                                'field-value_type' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_type',
                                                    ],
                                                ],
                                                'field-value_int' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_int',
                                                    ],
                                                ],
                                                'field-value_decimal' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_decimal',
                                                    ],
                                                ],
                                                'field-value_varchar' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_varchar',
                                                    ],
                                                ],
                                                'field-value_text' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_text',
                                                    ],
                                                ],
                                                'field-value_bool' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_bool',
                                                    ],
                                                ],
                                                'field-value_date' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_date',
                                                    ],
                                                ],
                                                'field-value_time' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_time',
                                                    ],
                                                ],
                                                'field-value_datetime' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_datetime',
                                                    ],
                                                ],
                                                'field-value_json' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_json',
                                                    ],
                                                ],
                                                'field-value_model_class' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_model_class',
                                                    ],
                                                ],
                                                'field-value_model_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'individualIndividualattributes.value_model_id',
                                                    ],
                                                ],
//                                                'field-individual_id' => [
//                                                    'class' => FormField::class,
//                                                    'settings' => [
//                                                        'field' => 'individualIndividualattributes.individual_id',
//                                                    ],
//                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }
    //endregion EditViewInterface implementation
}
